using UnityEngine;

namespace TurretModule.API
{
    public interface ITurretAmmunition
    {
        int Id { get; }
        Transform AmmunitionTransform { get; }
        void Init(Vector3 direction, float force);
    }
}