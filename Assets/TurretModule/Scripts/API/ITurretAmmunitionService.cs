using System.Collections.Generic;

namespace TurretModule.API
{
    public interface ITurretAmmunitionService
    {
        List<ITurretAmmunition> AmmunitionVariants { get; }
        ITurretAmmunition GetAmmunition(int id);
    }
}