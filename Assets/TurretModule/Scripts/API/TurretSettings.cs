using System.Collections.Generic;
using System.Linq;

namespace TurretModule.API
{
    public class TurretSettings
    {
        public float SqrtDistanceForUpdateRotate = 20;
        public float BaseAngleForRotateMiddle = 5;
        public float SpeedRotateBase = 5;
        public float SpeedRortateMiddle = 2;
        public bool IsShootingAhead = false;
        public float AngleShootingAhead = 5;
        
        public List<TurretWeaponSettings> Weapons;

        public TurretWeaponSettings GetWeaponSettingsById(int id) => 
            Weapons.FirstOrDefault(w => w.WeaponId.Equals(id));
    }

    public class TurretWeaponSettings
    {
        public int WeaponId;
        public int AmmunitionId;
    }
}