namespace TurretModule.API
{
    public interface IBaseTurretWeapon
    {
        int WeaponId { get; }
        void Init(TurretWeaponSettings weaponSettings, ITurretAmmunitionService ammunitionService);
        void TryShoot(ITurretEnemy enemy);
        void OnUpdate();
    }
}