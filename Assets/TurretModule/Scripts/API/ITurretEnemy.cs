using UnityEngine;

namespace TurretModule.API
{
    public interface ITurretEnemy
    {
        bool IsDead { get; }
        Vector3 GetAimPosition();
        Vector3 GetAimDirection();
    }
}