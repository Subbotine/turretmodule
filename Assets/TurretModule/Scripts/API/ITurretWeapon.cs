namespace TurretModule.API
{
    public interface ITurretWeapon<T> : IBaseTurretWeapon where T : TurretWeaponSettings
    {
        T WeaponSettings { get; set; }
    }
}