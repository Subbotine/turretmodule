using System;

namespace TurretModule.API
{
    public interface ITurretDetector
    {
        event Action<ITurretEnemy> OnEnemyEntered;
        event Action<ITurretEnemy> OnEnemyExit;
        ITurretEnemy GetEnemy();
    }
}