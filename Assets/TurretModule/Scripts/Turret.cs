using DG.Tweening;
using TurretModule.API;
using UnityEngine;

namespace TurretModule
{
    public class Turret : MonoBehaviour
    {
        [SerializeField] private Transform _baseTransform;
        [SerializeField] private Transform _middleTransform;
        [SerializeField] private Vector3 _defaultEulerMiddle;

        protected Vector3 LastPositionAim;
        protected ITurretEnemy TargetEnemy;
        protected TurretSettings Settings;
        protected ITurretDetector DetectArea;
        protected IBaseTurretWeapon[] Weapons;

        private bool _isRotating;
        private Tweener _baseRotateTween;
        private Tweener _middleRotateTween;
        private Transform _middleTransformParent;
        private Quaternion _lookAtUpTarget;

        private void Start()
        {
            _middleTransformParent = _middleTransform.parent;
        }

        private void Update()
        {
            if (Settings == null) return;

            LookAtEnemy();
            CheckConditionsShoot();
        }

        public void Init(TurretSettings settings, ITurretAmmunitionService ammunitionService,
            ITurretDetector detectArea)
        {
            Settings = settings;
            DetectArea = detectArea;
            Weapons = GetComponentsInChildren<IBaseTurretWeapon>();

            foreach (var weapon in Weapons)
            {
                var wSettings = Settings.GetWeaponSettingsById(weapon.WeaponId);
                if (wSettings == null) continue;
                weapon.Init(wSettings, ammunitionService);
            }

            DetectArea.OnEnemyEntered += OnEnemyEntered;
            DetectArea.OnEnemyExit += OnEnemyExit;
        }

        protected virtual Vector3 GetAimPosition()
        {
            if (TargetEnemy == null) return Vector3.zero;
            if (Settings.IsShootingAhead) return TargetEnemy.GetAimDirection();
            return TargetEnemy.GetAimPosition();
        }

        protected virtual void OnEnemyExit(ITurretEnemy enemy)
        {
            if (enemy != TargetEnemy) return;
            TargetEnemy = DetectArea.GetEnemy();
            if (TargetEnemy == null) LeanToDefault();
        }

        protected virtual void OnEnemyEntered(ITurretEnemy enemy)
        {
            TargetEnemy = enemy;
        }

        private void CheckConditionsShoot()
        {
            if (TargetEnemy == null) return;

            WeaponsUpdate();

            if (!Settings.IsShootingAhead && !_isRotating)
            {
                TryShoot();
                return;
            }

            if (!Settings.IsShootingAhead) return;

            var angle = Mathf.DeltaAngle(_lookAtUpTarget.eulerAngles.y, _baseTransform.eulerAngles.y);
            if (Mathf.Abs(angle) < Settings.AngleShootingAhead) TryShoot();
        }

        private void TryShoot()
        {
            foreach (var weapon in Weapons)
            {
                weapon.TryShoot(TargetEnemy);
            }
        }

        private void WeaponsUpdate()
        {
            foreach (var weapon in Weapons)
            {
                weapon.OnUpdate();
            }
        }

        private void LookAtEnemy()
        {
            var positionAim = GetAimPosition();

            var distanceToNewPositionAim = (LastPositionAim - positionAim).sqrMagnitude;
            if (LastPositionAim == positionAim) return;
            if (_baseRotateTween != null && _baseRotateTween.IsPlaying() &&
                distanceToNewPositionAim < Settings.SqrtDistanceForUpdateRotate) return;

            _isRotating = true;
            var direction = positionAim - _baseTransform.position;
            direction.y = 0;
            _lookAtUpTarget = Quaternion.LookRotation(direction, Vector3.up);

            var angle = Mathf.DeltaAngle(_lookAtUpTarget.eulerAngles.y, _baseTransform.eulerAngles.y);
            angle = Mathf.Abs(angle);
            var duration = GetDurationRotate(angle, Settings.SpeedRotateBase);

            if (angle <= Settings.BaseAngleForRotateMiddle)
                LeanToEnemy();

            if (_baseRotateTween != null && _baseRotateTween.IsPlaying())
            {
                _baseRotateTween.ChangeEndValue(_lookAtUpTarget.eulerAngles, duration, true).Restart();
            }
            else
            {
                _baseRotateTween = _baseTransform
                    .DORotate(_lookAtUpTarget.eulerAngles, duration)
                    .SetEase(Ease.Linear)
                    .OnComplete(LeanToEnemy).SetAutoKill(false);
            }

            LastPositionAim = positionAim;
        }

        private void LeanToEnemy()
        {
            var positionAim = GetAimPosition();

            var direction = positionAim - _middleTransform.position;
            direction = _middleTransformParent.InverseTransformVector(direction);
            direction.x = 0;
            var lookEuler = Quaternion.LookRotation(direction).eulerAngles;

            var angle = Mathf.DeltaAngle(lookEuler.x, _middleTransform.localEulerAngles.x);
            var duration = GetDurationRotate(Mathf.Abs(angle), Settings.SpeedRortateMiddle);

            if (_middleRotateTween != null && _middleRotateTween.IsPlaying())
            {
                _middleRotateTween.ChangeEndValue(lookEuler, duration, true).Restart();
            }
            else
            {
                _middleRotateTween = _middleTransform
                    .DOLocalRotate(lookEuler, duration)
                    .SetEase(Ease.Linear).OnComplete(
                        () => { _isRotating = false; }).SetAutoKill(false);
            }
        }

        private void LeanToDefault()
        {
            var angle = Mathf.DeltaAngle(_defaultEulerMiddle.x, _middleTransform.localEulerAngles.x);
            var duration = GetDurationRotate(Mathf.Abs(angle), Settings.SpeedRortateMiddle);

            if (_middleRotateTween != null && _middleRotateTween.IsPlaying())
            {
                _middleRotateTween.ChangeEndValue(_defaultEulerMiddle, duration, true).Restart();
            }
            else
            {
                _middleRotateTween = _middleTransform
                    .DOLocalRotate(_defaultEulerMiddle, duration)
                    .SetEase(Ease.Linear).OnComplete(
                        () => { _isRotating = false; }).SetAutoKill(false);
            }
        }

        private float GetDurationRotate(float angle, float speed)
        {
            var percent = angle / 360;
            return percent * speed;
        }
    }
}
