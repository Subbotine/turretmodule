using System;
using System.Collections.Generic;
using System.Linq;
using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class TurretDetectArea : MonoBehaviour, ITurretDetector
    {
        public event Action<ITurretEnemy> OnEnemyEntered;
        public event Action<ITurretEnemy> OnEnemyExit;
        
        private List<ITurretEnemy> _enemies = new List<ITurretEnemy>();
        
        public virtual ITurretEnemy GetEnemy()
        {
            if (_enemies.Count == 0) return null;
            return _enemies.First();
        }

        private void OnTriggerEnter(Collider other)
        {
            var enemy = other.GetComponent<ITurretEnemy>();
            if(enemy == null) return;
            if(_enemies.Contains(enemy)) return;
            _enemies.Add(enemy);
            OnEnemyEntered?.Invoke(enemy);
        }

        private void OnTriggerExit(Collider other)
        {
            var enemy = other.GetComponent<ITurretEnemy>();
            if(enemy == null) return;
            if(!_enemies.Contains(enemy)) return;
            _enemies.Remove(enemy);
            OnEnemyExit?.Invoke(enemy);
        }
    }
}