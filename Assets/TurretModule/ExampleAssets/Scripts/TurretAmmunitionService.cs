using System.Collections.Generic;
using System.Linq;
using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class TurretAmmunitionService : MonoBehaviour, ITurretAmmunitionService
    {
        [SerializeField] private List<PolygonProjectileScript> _bulletsPrefabs;

        private List<ITurretAmmunition> _ammunitionVariantsCache;

        public List<ITurretAmmunition> AmmunitionVariants
        {
            get
            {
                if (_ammunitionVariantsCache == null)
                {
                    _ammunitionVariantsCache = _bulletsPrefabs.Select(a => (ITurretAmmunition)a).ToList();
                }

                return _ammunitionVariantsCache;
            }
        }

        public ITurretAmmunition GetAmmunition(int id)
        {
            var ammunition = AmmunitionVariants.FirstOrDefault(a => a.Id.Equals(id));
            if (ammunition == null) Debug.LogError($"Ammunition's id = {id} didn't find!");
            return ammunition;
        }
    }
}