using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class TurretRocketWeapon : MonoBehaviour, ITurretWeapon<ExampleTurretWeaponSettings>
    {
        [SerializeField] private int _weaponId;
        [SerializeField] private Transform _shootPoint;
        [SerializeField] private Transform _base;
        [SerializeField] private float _interval = 2f;
        
        private ITurretAmmunitionService _ammunitionService;
        private float _timer;

        public int WeaponId => _weaponId;
        public ExampleTurretWeaponSettings WeaponSettings { get; set; }

        public void Init(TurretWeaponSettings weaponSettings, ITurretAmmunitionService ammunitionService)
        {
            WeaponSettings = (ExampleTurretWeaponSettings)weaponSettings;
            _ammunitionService = ammunitionService;
        }

        public void TryShoot(ITurretEnemy enemy)
        {
            if(enemy == null) return;
            
            if(_timer < _interval) return;
            _timer = 0;

            var lookEnemyQuaternion = Quaternion.LookRotation(enemy.GetAimPosition() - _base.transform.position);
            _base.transform.eulerAngles = lookEnemyQuaternion.eulerAngles;
            
            var ammunition = _ammunitionService.GetAmmunition(WeaponSettings.AmmunitionId);
            if(ammunition == null || !(ammunition is PolygonProjectileScript)) return;

            var ammunitionMonoBeh = ammunition as PolygonProjectileScript;
            
            var direction = _shootPoint.forward;
            var spawnPositionWithOffset = _shootPoint.position + direction * WeaponSettings.SpawnAmmunitionOffset;
            var projectile = Instantiate(ammunitionMonoBeh, spawnPositionWithOffset, Quaternion.identity);
            var directionSpeed = direction * 10f;
            
            projectile.Init(directionSpeed, WeaponSettings.AmmunitionSpeed);
        }

        public void OnUpdate()
        {
            _timer += Time.deltaTime;
        }
    }
}