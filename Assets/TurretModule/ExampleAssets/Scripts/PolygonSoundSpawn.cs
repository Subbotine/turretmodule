﻿using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class PolygonSoundSpawn : MonoBehaviour
    {

        [SerializeField] private GameObject _prefabSound;

        [SerializeField] private bool _destroyWhenDone = true;
        [SerializeField] private bool _soundPrefabIsChild;
        [Range(0.01f, 10f), SerializeField] private float _pitchRandomMultiplier = 1f;

        private void Start()
        {
            GameObject sound = Instantiate(_prefabSound, transform.position, Quaternion.identity);
            AudioSource source = sound.GetComponent<AudioSource>();

            if (_soundPrefabIsChild)
                sound.transform.SetParent(transform);

            if (_pitchRandomMultiplier != 1)
            {
                if (Random.value < .5)
                    source.pitch *= Random.Range(1 / _pitchRandomMultiplier, 1);
                else
                    source.pitch *= Random.Range(1, _pitchRandomMultiplier);
            }

            if (_destroyWhenDone)
            {
                float life = source.clip.length / source.pitch;
                Destroy(sound, life);
            }
        }
    }
}
