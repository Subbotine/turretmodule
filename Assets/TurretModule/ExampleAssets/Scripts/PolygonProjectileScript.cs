﻿using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class PolygonProjectileScript : MonoBehaviour, ITurretAmmunition
    {
        [SerializeField] private int _id;
        
        [SerializeField] private GameObject _impactParticle;
        [SerializeField] private GameObject _projectileParticle;
        [SerializeField] private GameObject _muzzleParticle;
        [SerializeField] private GameObject[] _trailParticles;
        [Range(0f, 1f), SerializeField] private float _collideOffset = 0.15f;

        private Rigidbody _rb;
        private Transform _myTransform;

        private bool _destroyed;

        public int Id => _id;
        public Transform AmmunitionTransform => _myTransform;

        private void Update()
        {
            if (_destroyed) return;
            RotateTowardsDirection();
        }

        public void Init(Vector3 direction, float force)
        {
            _rb = GetComponent<Rigidbody>();
            _myTransform = transform;

            var lookAt = Quaternion.LookRotation(direction);
            _myTransform.rotation = lookAt;
            _rb.AddForce(direction * force);
            
            _projectileParticle = Instantiate(_projectileParticle, _myTransform.position, _myTransform.rotation);
            _projectileParticle.transform.parent = _myTransform;
            
            if (_muzzleParticle)
            {
                _muzzleParticle = Instantiate(_muzzleParticle, _myTransform.position, _myTransform.rotation);
                Destroy(_muzzleParticle, 1.5f);
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            _myTransform.position = other.GetContact(0).point + (other.GetContact(0).normal * _collideOffset);
            GameObject impactP = Instantiate(_impactParticle, _myTransform.position, Quaternion.FromToRotation(Vector3.up, other.GetContact(0).normal));
            Destroy(impactP, 5.0f);
            DestroyMissile();
        }

        private void DestroyMissile()
        {
            _destroyed = true;

            foreach (GameObject trail in _trailParticles)
            {
                GameObject curTrail = _myTransform.Find(_projectileParticle.name + "/" + trail.name).gameObject;
                curTrail.transform.parent = null;
                Destroy(curTrail, 3f);
            }
            Destroy(_projectileParticle, 3f);
            Destroy(gameObject);

            ParticleSystem[] trails = GetComponentsInChildren<ParticleSystem>();
            
            for (int i = 1; i < trails.Length; i++)
            {
                ParticleSystem trail = trails[i];
                if (trail.gameObject.name.Contains("Trail"))
                {
                    trail.transform.SetParent(null);
                    Destroy(trail.gameObject, 2f);
                }
            }
        }

        private void RotateTowardsDirection()
        {
            if (_rb.velocity != Vector3.zero)
            {
                Quaternion targetRotation = Quaternion.LookRotation(_rb.velocity.normalized, Vector3.up);
                float angle = Vector3.Angle(_myTransform.forward, _rb.velocity.normalized);
                float lerpFactor = angle * Time.deltaTime;
                _myTransform.rotation = Quaternion.Slerp(_myTransform.rotation, targetRotation, lerpFactor);
            }
        }
    }
}
