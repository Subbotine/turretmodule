using TurretModule.API;

namespace TurretModule.ExampleAssets
{
    public class ExampleTurretWeaponSettings : TurretWeaponSettings
    {
        public float SpawnAmmunitionOffset = 0.3f;
        public float AmmunitionSpeed = 1000;
    }
}