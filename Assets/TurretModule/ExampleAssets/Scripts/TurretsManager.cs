using System.Collections.Generic;
using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class TurretsManager : MonoBehaviour
    {
        [SerializeField] private TurretAmmunitionService _ammunitionService;
        [SerializeField] private TurretDetectArea _detectArea;
        [SerializeField] private Turret[] _turrets;
        [SerializeField] private bool _isShootingAhead;

        private void Start()
        {
            InitTurrets();
        }

        private void InitTurrets()
        {
            var settings = new TurretSettings
            {
                IsShootingAhead = _isShootingAhead,
                Weapons = new List<TurretWeaponSettings>()
            };
            
            var weaponSettings = new ExampleTurretWeaponSettings
            {
                WeaponId = 0,
                AmmunitionId = 0
            };
            
            var weaponRocketSettings = new ExampleTurretWeaponSettings
            {
                WeaponId = 1,
                AmmunitionId = 1,
                AmmunitionSpeed = 500
            };
            
            settings.Weapons.Add(weaponSettings);
            settings.Weapons.Add(weaponRocketSettings);
            
            foreach (var turret in _turrets)
            {
                turret.Init(settings, _ammunitionService, _detectArea);
            }
        }
    }
}