using TurretModule.API;
using UnityEngine;

namespace TurretModule.ExampleAssets
{
    public class TurretEnemy : MonoBehaviour, ITurretEnemy
    {
        public bool IsDead => false;
        public Vector3 GetAimPosition() => transform.position;

        public Vector3 GetAimDirection()
        {
            var transformAim = transform;
            return transformAim.position + transformAim.forward.normalized;
        }
    }
}