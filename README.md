# TurretModule

## UML Diagram
https://drive.google.com/file/d/1xGhi49CxK93uJRN290aty_oV8XYZc0PH/view?usp=sharing


## Getting started
Integrate the "TurretModule" folder into your project, look at the example turret implementation in the "ExampleAssets" subfolder. After studying the example assets with scripts inside, you can write your own implementation of the necessary parts and delete the example folder.
For a better understanding of the system, you can see the UML diagram at the link above.